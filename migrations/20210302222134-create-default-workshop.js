'use strict';
const moment = require('moment');

const createdAt = moment().format('YYYY-MM-DD HH:mm:ss');
const updatedAt = moment().format('YYYY-MM-DD HH:mm:ss');


const getUserAdmin = async (queryInterface) => {
  try {
    const [[{ id }]] = await queryInterface.sequelize.query(`SELECT id from users where profile = 'admin' limit 1;`);
    return id;
  } catch (err) {
    throw new Error(err.message);
  }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const userId = await getUserAdmin(queryInterface);
      console.log(userId);
      const bulkData = [
        {
          userId,
          name: 'Workshop 1',
          address: 'La moneda',
          phone: '+56974321234',
          createdAt,
          updatedAt
        }
      ]
      await queryInterface.bulkInsert('workshops', bulkData, { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err.message;


    }
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
