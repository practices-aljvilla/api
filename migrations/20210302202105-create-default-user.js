'use strict';
const crypto = require('crypto');
const moment = require('moment');

const createdAt = moment().format('YYYY-MM-DD HH:mm:ss');
const updatedAt = moment().format('YYYY-MM-DD HH:mm:ss');

const { SECRET } = process.env;
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const password = crypto.createHmac('sha256', SECRET)
        .update('1234')
        .digest('hex');
      const bulkData = [
        {
          userName: 'workshop',
          password,
          profile: 'admin',
          createdAt,
          updatedAt
        }
      ]
      await queryInterface.bulkInsert('users', bulkData, { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err.message;


    }
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
