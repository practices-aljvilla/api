This README would normally document whatever steps are necessary to get the application up and running.

Things you may want to cover:

## Requirements

- node v14

- Some data base (Postgres, MySQL, MariaDB, SQLite or Microsoft SQL Server)
- git
 

## First run

Note: Only tested on Mac OS X and Linux so far.

Assuming you have a installed all requirements:

  

```
$ git clone https://aljvilla@bitbucket.org/practices-aljvilla/api.git
```
```
$ cd api
```
```
$ npm i
```
Copy with a new name .env.sample file and set env values:
```
PG_USER=
PG_PW=
PG_DB=
PG_HOST=
SEQUELIZE_DB_DIALECT=
PG_SOCKET=
SECRET=
PORT=
TOKEN_SECRET=
TOKEN_EXPIRES=
```
```
$ npm run dev
```

It will:
After it finishes you will see something like this on your console:
```

[nodemon] 2.0.7
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node src/index.js`
app Let's rock!! 🤘🏻🚀
app Server running at http://127.0.0.1:3500/
```
