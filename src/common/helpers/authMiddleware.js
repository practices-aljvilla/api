module.exports = ({ helpers }) => async (req, res, next) => {
  try {
    const token = req.headers.authorization;
    if (!token) {
      return res.status(400).json({ error: 'token header is required' });
    }
    const user = helpers.jwt.verify(token);
    req.userRequest = user;
    return next();
  } catch (err) {
    if(err.message.includes('jwt expired')){
      return res.sendStatus(401);
    }
    return res.status(500).json({ error: err.message });
  }
}