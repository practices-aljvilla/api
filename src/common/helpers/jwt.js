'use strict';

const jwt = require('jsonwebtoken');

const { TOKEN_SECRET, TOKEN_EXPIRES } = process.env;

module.exports = {
  createToken: (value) => jwt.sign(value, TOKEN_SECRET, { expiresIn: TOKEN_EXPIRES }),
  verify: (value) => jwt.verify(value, TOKEN_SECRET),
};