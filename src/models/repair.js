'use strict';

const sequelizePaginate = require('sequelize-paginate');
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class repair extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      models.repair.belongsTo(models.car, { foreignKey: 'carId' });
    }
  };
  repair.init({
    description: DataTypes.STRING,
    carId: DataTypes.INTEGER,
    price: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'repair',
    timestamps: true,
    paranoid: true,
  });
  sequelizePaginate.paginate(repair);
  return repair;
};