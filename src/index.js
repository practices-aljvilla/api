'use strict';

require('dotenv').config();

const express = require('express')
const Joi = require('joi');
const helmet = require('helmet');
const cors = require('cors');
const crypto = require('crypto');

const helpers = require('common/helpers');
const models = require('models');
const routes = require('modules');
const app = express();
app.use(express.json({ extended: true }));
app.use(helmet());
app.use(cors());


app.use('', routes({ helpers, models, Joi, crypto }));
app.use((req, res) => {
  return res.status(404).send('path not found')
});
const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`app Let's rock!! 🤘🏻🚀`)
  console.log(`app Server running at http://localhost:${port}`)
})