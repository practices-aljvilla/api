'use strict';
const clientsList = require('modules/client/list');
const carsList = require('modules/client/carsList');
const saveClient = require('modules/client/saveClient');

module.exports = (dependencies) => {
  return {
    'client': [
      {
        method: 'post',
        path: '/',
        ctrlMiddleware: saveClient(dependencies),
      },
      {
        method: 'get',
        path: '/',
        ctrlMiddleware: clientsList(dependencies),
      },
      {
        method: 'get',
        path: '/:clientId/cars',
        ctrlMiddleware: carsList(dependencies),
      }
    ],
  };
}
