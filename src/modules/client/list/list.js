'use strict';

module.exports = ({ models }) => async (req, res) => {
  try {
    const {
      userRequest: { workshop: { id: workshopId } },
      query: { page = 1, paginate = 2 } } = req;
    const clients = await models.client
      .paginate({
        where: { workshopId },
        page,
        paginate,
        order: [['createdAt', 'DESC']],
        attributes: [
          'id',
          'fullName',
          'identificationValue',
          'phone',
          'email',
        ]
      });
    return res.json(clients);
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}