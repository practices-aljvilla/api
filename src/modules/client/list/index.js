'use strict';

const usersList = require('modules/client/list/list');


module.exports = (dependencies) => [
  dependencies.helpers.authMiddleware(dependencies),
  usersList(dependencies),
];