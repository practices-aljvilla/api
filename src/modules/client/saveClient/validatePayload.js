'use strict';

const validateSchema = ({ Joi }) => {
  return Joi.object({
    fullName: Joi.string().required(),
    identificationValue: Joi.string().required(),
    phone: Joi.string().required(),
    email: Joi.string().required().email(),
  });
}

module.exports = ({ Joi }) => async (req, res, next) => {
  try {
    const validateResponse = validateSchema({ Joi }).validate(req.body);
    if (validateResponse.error) {
      return res.status(400).json({ error: validateResponse.error.message });
    }
    return next();
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}