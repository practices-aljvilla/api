'use strict';

const validatePayload = require('modules/client/saveClient/validatePayload');
const save = require('modules/client/saveClient/save');

module.exports = (dependencies) => [
  dependencies.helpers.authMiddleware(dependencies),
  validatePayload(dependencies),
  save(dependencies),
];