'use strict';

module.exports = ({ models }) => async (req, res) => {
  try {
    const { body, userRequest: { workshop: { id: workshopId } } } = req;
    const clientData = { ...body, workshopId };
    await models.client.create(clientData);
    return res.sendStatus(201);
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}