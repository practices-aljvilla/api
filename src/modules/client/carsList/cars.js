'use strict';

module.exports = ({ models }) => async (req, res) => {
  try {
    const { params: { clientId } } = req;
    const cars = await models.car.findAll({ where: { clientId } });
    return res.json(cars.map((car) => {
      const { createdAt, updatedAt, deletedAt, clientId, ...item } = car.dataValues;
      return item;
    }));
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}