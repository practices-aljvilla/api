'use strict';

const carsList = require('modules/client/carsList/cars');


module.exports = (dependencies) => [
  dependencies.helpers.authMiddleware(dependencies),
  carsList(dependencies)
]