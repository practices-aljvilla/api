'use strict';

module.exports = ({ models }) => async (req, res, next) => {
  try {
    const carData = req.body;
    await models.car.create(carData);
    return res.sendStatus(201);
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}