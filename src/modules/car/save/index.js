'use strict';

const validatePayload = require('modules/car/save/validatePayload');
const save = require('modules/car/save/save');

module.exports = (dependencies) => [
  dependencies.helpers.authMiddleware(dependencies),
  validatePayload(dependencies),
  save(dependencies),
];