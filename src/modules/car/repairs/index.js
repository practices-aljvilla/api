'use strict';
const repairs = require('modules/car/repairs/repairs');

module.exports = (dependencies) => [
  dependencies.helpers.authMiddleware(dependencies),
  repairs(dependencies),
]