'use strict';

module.exports = ({ models }) => async (req, res) => {
  try {
    const { params: { carId } } = req;
    const reapirs = await models.repair.findAll({ where: { carId } });
    return res.json(reapirs.map((reapir) => {
      const {
        createdAt,
        updatedAt,
        deletedAt,
        ...item
      } = reapir.dataValues;
      return item;
    }));
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}