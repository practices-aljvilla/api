'use strict';
const repairs = require('modules/car/repairs');
const save = require('modules/car/save');

module.exports = (dependencies) => {
  return {
    'car': [
      {
        method: 'post',
        path: '/',
        ctrlMiddleware: save(dependencies),
      },
      {
        method: 'get',
        path: '/:carId/repairs',
        ctrlMiddleware: repairs(dependencies),
      },
    ],
  };
}
