'use strict';
const fs = require('fs');
const express = require('express');

const router = express.Router();
const basePath = __dirname;
module.exports = (dependencies) => {
  fs.readdirSync(basePath).forEach((dirName) => {
    const dirPath = `${basePath}/${dirName}`;
    if (fs.lstatSync(dirPath).isDirectory()) {
      const ctrlPaths = require(dirPath)(dependencies);
      for (const key in ctrlPaths) {
        if (ctrlPaths.hasOwnProperty(key)) {
          ctrlPaths[key].forEach(({ method, path, ctrlMiddleware }) => {
            const middleware = [];
            if (ctrlMiddleware) middleware.push(...ctrlMiddleware);
            router.route(`/${key}${path}`)[method](...middleware);
          });
        }
      }
    }
  });
  return router;
};