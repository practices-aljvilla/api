'use strict';
const login = require('modules/user/login');

module.exports = (dependencies) => {
  return {
    'user': [
      {
        method: 'post',
        path: '/login',
        ctrlMiddleware: login(dependencies),
      },
    ],
  };
}
