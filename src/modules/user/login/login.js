'use strict';

const { SECRET } = process.env;

module.exports = ({ models, crypto, }) => async (req, res, next) => {
  try {
    const { body: { email: userName, password } } = req;
    const user = await models.user.findOne({ where: { userName }, include: [{ model: models.workshop }] });
    if (!user) {
      return res.status(400).json({ error: 'User or password incorrect' });
    }
    const passwordEmcrypted = crypto.createHmac('sha256', SECRET)
      .update(password)
      .digest('hex');
    if (passwordEmcrypted !== user.password) {
      return res.status(400).json({ error: 'User or password incorrect' });
    }
    res.userData = user;
    return next();
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}