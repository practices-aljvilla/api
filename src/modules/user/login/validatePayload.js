'use strict';

const validateSchema = ({ Joi }) => {
  return Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().required(),
  });
}

module.exports = ({ Joi }) => async (req, res, next) => {
  try {
    const validateResponse = validateSchema({ Joi }).validate(req.body);
    if (validateResponse.error) {
      return res.status(400).json({ error: validateResponse.error.message });
    }
    return next();
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}