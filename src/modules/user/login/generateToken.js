'use strict';

module.exports = ({ helpers }) => async (req, res) => {
  try {
    const { userId, address, phone, createdAt, updatedAt, deletedAt, ...workshop } = res.userData.workshop.dataValues;
    const {  userName, profile, id  } = res.userData;
    const token = helpers.jwt.createToken({ userName, profile, id, workshop });
    return res.json({ token })
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}