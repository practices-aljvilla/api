'use strict';

const validatePayload = require('modules/user/login/validatePayload');
const login = require('modules/user/login/login');
const generateToken = require('modules/user/login/generateToken');

module.exports = (dependencies) => [
  validatePayload(dependencies),
  login(dependencies),
  generateToken(dependencies),
]