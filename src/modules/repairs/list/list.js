module.exports = ({ models }) => async (req, res) => {
  try {
    const { page = 1, paginate = 20 } = req.query;
    const repairs = await models.repair.paginate({
      page,
      paginate,
      order: [['createdAt', 'DESC']],
      attributes: [
        'id',
        'description',
        'carId',
        'price',
        'createdAt',
      ]
    });
    return res.json(repairs);
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}