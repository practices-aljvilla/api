'use strict';

const list = require('modules/repairs/list/list');

module.exports = (dependencies) => [
  dependencies.helpers.authMiddleware(dependencies),
  list(dependencies),
];