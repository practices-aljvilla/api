'use strict';
const save = require('modules/repairs/save');
const list = require('modules/repairs/list');

module.exports = (dependencies) => {
  return {
    'repairs': [
      {
        method: 'post',
        path: '/',
        ctrlMiddleware: save(dependencies),
      },
      {
        method: 'get',
        path: '/',
        ctrlMiddleware: list(dependencies),
      },
    ],
  };
}
