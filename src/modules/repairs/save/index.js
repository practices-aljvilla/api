'use strict';

const save = require('modules/repairs/save/save');
const validatePayload = require('modules/repairs/save/validatePayload');

module.exports = (dependencies) => [
  dependencies.helpers.authMiddleware(dependencies),
  validatePayload(dependencies),
  save(dependencies),
];