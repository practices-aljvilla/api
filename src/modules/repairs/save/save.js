'use strict';

module.exports = ({ models }) => async (req, res) => {
  try {
    const { body } = req;
    await models.repair.create(body);
    return res.sendStatus(201);
  } catch (err) {
    return res.status(500).json({ error: err.message });
  }
}